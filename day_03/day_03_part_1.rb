file_path = File.expand_path('day_03_input.txt', __dir__)
input = File.read(file_path)

diagnostics = input.split("\n")

gamma_rates = []
epsilon_rates = []
gamma_rate = ''
epsilon_rate = ''

diagnostics.each_with_index do |diagnostic|
  diagnostic.split('').each_with_index do |char, i|
    char == '1' ? gamma_rates[i] = (gamma_rates[i] || 0) + 1 : epsilon_rates[i] = (epsilon_rates[i] || 0) + 1
  end
end

gamma_rates.each_with_index do |gamma, i|
  if gamma_rates[i] > epsilon_rates[i]
    gamma_rate += '1'
    epsilon_rate += '0'
  else
    gamma_rate += '0'
    epsilon_rate += '1'
  end
end

p gamma_rate.to_i(2) * epsilon_rate.to_i(2)
