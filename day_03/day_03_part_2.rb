file_path = File.expand_path('day_03_input.txt', __dir__)
input = File.read(file_path)

diagnostics_1 = input.split("\n")
diagnostics_2 = diagnostics_1

i = 0

while diagnostics_1.length > 1 || diagnostics_2.length > 1
  if diagnostics_1.length > 1
    ones_1 = diagnostics_1.count { |d| d[i] == '1' }
    zeros_1 = diagnostics_1.count { |d| d[i] == '0' }

    common = ones_1 >= zeros_1 ? '1' : '0'

    diagnostics_1 = diagnostics_1.filter { |n| n[i] == common }
  end

  if diagnostics_2.length > 1
    ones_2 = diagnostics_2.count { |n| n[i] == '1' }
    zeros_2 = diagnostics_2.count { |n| n[i] == '0' }

    common = ones_2 >= zeros_2 ? '0' : '1'

    diagnostics_2 = diagnostics_2.filter { |n| n[i] == common }
  end

  i += 1
end

oxygen_rating = diagnostics_1[0].to_i(2)
c02_rating = diagnostics_2[0].to_i(2)

puts oxygen_rating * c02_rating
