def read_file
  file_path = File.expand_path('day_06_input.txt', __dir__)
  File.read(file_path)
end

def lanterfish_template
  {
    0 => 0,
    1 => 0,
    2 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0
  }
end

def number_of_fish(lanternfish)
  fish_count = 0

  lanternfish.each do |fish|
    fish_count += fish[1]
  end

  fish_count
end

lanternfish = lanterfish_template
day = 0

read_file.split(',').each do |fish|
  lanternfish[fish.to_i] = lanternfish[fish.to_i] + 1
end

while day != 256
  new_lanternfish = lanterfish_template

  lanternfish.each do |fish|
    days = fish[0]
    amount = fish[1]

    next if amount.zero?

    if days.zero?
      [6, 8].each { |i| new_lanternfish[i] += amount }
    else
      new_lanternfish[days - 1] += amount
    end
  end

  lanternfish = new_lanternfish

  day += 1
end

fish_count = 0

lanternfish.each do |fish|
  fish_count += fish[1]
end

p number_of_fish(lanternfish)
