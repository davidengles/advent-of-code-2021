def read_file
  file_path = File.expand_path('day_07_input.txt', __dir__)
  File.read(file_path)
end

crabs = read_file.split(',').map(&:to_i)
a, b = [crabs.min, crabs.max]
fuel_cost = []

while a <= b
  crabs.each do |crab|
    fuel_cost[a] = fuel_cost.fetch(a, 0) + (a - crab).abs
  end

  a += 1
end

cost, x = fuel_cost.each_with_index.min
p "#{x} | #{cost}"
