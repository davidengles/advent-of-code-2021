file_path = File.expand_path('day_02_input.txt', __dir__)
input = File.read(file_path)

commands = input.split("\n")

horizontal_position = 0
depth = 0

commands.each do |command|
  direction, distance = command.split(' ')
  distance = distance.to_i

  case direction
  when 'forward'
    horizontal_position += distance
  when 'down'
    depth += distance
  when 'up'
    depth -= distance
  end
end

p horizontal_position * depth
