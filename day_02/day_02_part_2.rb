file_path = File.expand_path('day_02_input.txt', __dir__)
input = File.read(file_path)

commands = input.split("\n")

horizontal_position = 0
depth = 0
aim = 0

commands.each do |command|
  direction, distance = command.split(' ')
  distance = distance.to_i

  case direction
  when 'forward'
    horizontal_position += distance
    depth += (aim * distance)
  when 'down'
    aim += distance
  when 'up'
    aim -= distance
  end
end

p horizontal_position * depth
