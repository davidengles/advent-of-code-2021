file_path = File.expand_path('day_04_input.txt', __dir__)
input = File.read(file_path)

bingo = input.split("\n")

draws = bingo[0].strip.split(',').map(&:to_i)
cards = bingo[1..-1].reject { _1 == '' }.map(&:strip).each_slice(5).map do |card|
  card.map do |line|
    line.split(' ').map do |number|
      [number.to_i, false]
    end
  end
end

winner = false
current_draw = 0
winning_cards = []
winning_card = nil
score = 0

until winner
  cards.each_with_index do |card, i|
    card.each_with_index do |line, j|
      line.each_with_index do |number, k|
        cards[i][j][k][1] = true if draws[current_draw] == number[0]
      end
    end

    card.each do |line|
      winner = true if line.select { _1[1] == true }.count == line.count
    end

    card.transpose.each do |line|
      winner = true if line.select { _1[1] == true }.count == line.count
    end

    winning_cards << i if winner && !winning_cards.include?(i)
    if winning_cards.length == cards.length
      winning_card = card
      break
    end

    winner = false
  end

  winner = winning_cards.length == cards.length

  current_draw += 1
end

winning_card.each do |line|
  line.each do |number|
    score += number[0] unless number[1]
  end
end

p draws[current_draw - 1] * score
