def read_file
  file_path = File.expand_path('day_05_input.txt', __dir__)
  File.read(file_path)
end

def mark_map(map, x, y)
  map[[x, y]] = map[[x, y]] + 1
end

# I normally 'improve' part one for my part two, but I was getting some weird results
# so I decided to start from scratch and ended up with something that is much more elegant
# for both parts. I've decided to keep part one as is for posterity though.

vectors = read_file.split("\n")

map = Hash.new(0)

vectors.each do |vector|
  x, y = vector.split(' -> ')
  x1, x2 = x.split(',')
  y1, y2 = y.split(',')
  x1 = x1.to_i
  x2 = x2.to_i
  y1 = y1.to_i
  y2 = y2.to_i

  xd = y1 <=> x1
  yd = y2 <=> x2
  x = x1
  y = x2

  while x != y1
    mark_map(map, x, y)
    x += xd
    y += yd
  end

  while y != y2
    mark_map(map, x, y)
    x += xd
    y += yd
  end
  mark_map(map, x, y)
end

p (map.values.count { |x| x > 1 })
