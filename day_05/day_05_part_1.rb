def read_file
  file_path = File.expand_path('day_05_input.txt', __dir__)
  File.read(file_path)
end

vectors = read_file.split("\n")

map = {}

vectors.each do |vector|
  start_point, end_point = vector.split(' -> ')
  x1, x2 = start_point.split(',')
  x1 = x1.to_i
  x2 = x2.to_i
  y1, y2 = end_point.split(',')
  y1 = y1.to_i
  y2 = y2.to_i

  if x1 == y1
    ([x2, y2].min..[x2, y2].max).step(1) do |i|
      map[[x1, i]] = map.fetch([x1, i], 0) + 1
    end
  elsif x2 == y2
    ([x1, y1].min..[x1, y1].max).step(1) do |i|
      map[[i, y2]] = map.fetch([i, y2], 0) + 1
    end
  end
end

p (map.values.count { |x| x > 1 })
