file_path = File.expand_path('day_01_input.txt', __dir__)
input = File.read(file_path)

depths = input.split("\n").map(&:to_i)
count = 0

depths.each_with_index do |x, i|
  count += 1 if x > depths[i - 1]
end

p count
