file_path = File.expand_path('day_01_input.txt', __dir__)
input = File.read(file_path)

depths = input.split("\n").map(&:to_i)
sets = []
count = 0

depths.each_with_index do |x, i|
  next if depths[i + 2].nil?

  sets << (x + depths[i + 1] + depths[i + 2])
end

sets.each_with_index do |x, i|
  count += 1 if x > sets[i - 1]
end

p count
